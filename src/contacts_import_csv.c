
/*
 * Osmo - a handy personal organizer
 *
 * Copyright (C) 2007 Tomasz Maka <pasp@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "contacts_import_csv.h"

#ifdef CONTACTS_ENABLED

#define FIELD_SEPARATOR ','
#define LINE_SEPARATOR '\n'

/*------------------------------------------------------------------------------*/
static gint
find_offset(const gchar *text, gsize len, gchar separator, gint nth) {
    gint i, count;
    gboolean quotation_found;

    for (i = 0, count = 1, quotation_found = FALSE; i < len; i++) {
        gchar c = text[i];
        if (count == nth) {
            return i;
        }
        if (c == '"') {
            quotation_found = !quotation_found;
        } else if (c == separator && quotation_found == FALSE) {
            count++;
        }
    }
    return -1;
}

/*------------------------------------------------------------------------------*/
static gchar *
substring_fragment(const gchar *text, gsize text_len, gchar separator, gint nth) {
    gint start = find_offset(text, text_len, separator, nth);
    if (start >= 0) {
        gint end = find_offset(text + start, text_len - start, separator, 2);
        if (end >= 0) {
            end = start + end - 1;
        } else {
            end = text_len;
        }
        gchar *buffer = g_new(gchar, (end - start) + 1);
        if (buffer) {
            g_strlcpy(buffer, text + start, (end - start)+1);
            return buffer;
        }
    }
    return NULL;

}

/*------------------------------------------------------------------------------*/

gchar *
csv_get_line(gint line, GUI *appGUI) {
    gchar *line_buffer = substring_fragment(appGUI->cnt->file_buffer, appGUI->cnt->file_length, LINE_SEPARATOR, line);
    if (line_buffer) {
        gint i, j;
        gsize len = strlen(line_buffer);
        for (i = 0, j = 0; i < len; i++) {
            if (line_buffer[i] == '\r') {
                /* no \r in memory */
                continue;
            }
            line_buffer[j++] = line_buffer[i];
        }
        line_buffer[j] = '\0';
        return line_buffer;
    }
    return NULL;
}

/*------------------------------------------------------------------------------*/

gchar *
csv_get_field(const gchar *line_buffer, gint field) {
    gchar *value = substring_fragment(line_buffer, strlen(line_buffer), FIELD_SEPARATOR, field);
    if (value) {
        gint i, j;
        gsize len = strlen(value);
        for (i = 0, j = 0; i < len; i++) {
            if (value[i] == '"') {
                if (value[i + 1] == '"') {
                    i++;
                } else {
                    continue;
                }
            }
            value[j++] = value[i];
        }
        value[j] = '\0';
        return value;
    }
    return NULL;
}

/*------------------------------------------------------------------------------*/

guint
get_number_of_records (GUI *appGUI) {

guint lines = 0, i;
gboolean quotation_found = FALSE, has_content = FALSE;

    for (i=0; i != appGUI->cnt->file_length; i++) {
        gchar c = appGUI->cnt->file_buffer[i];
        if (c == LINE_SEPARATOR && quotation_found == FALSE) {
            has_content = FALSE;
        } else if (!has_content) {
            lines++;
            has_content = TRUE;
        }
        if (c == '"') {
            quotation_found = !quotation_found;
        }
    }

    return lines;
}

/*------------------------------------------------------------------------------*/

#endif  /* CONTACTS_ENABLED */

